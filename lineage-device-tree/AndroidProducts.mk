#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_OnePlus6.mk

COMMON_LUNCH_CHOICES := \
    lineage_OnePlus6-user \
    lineage_OnePlus6-userdebug \
    lineage_OnePlus6-eng
